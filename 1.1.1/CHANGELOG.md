# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/sv/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
- Lägger till exempel på datamängd i CSV-, ODS- och XLSX-format.
- Lägger till CONTRIBUTING.md
### Changed
- Migrering från Versionshistorik till CHANGELOG.
### Fixed
- Bugg från komma till semikolon i exempel.
- Rättar till lista över Bidragsgivare från remissprocesser.


## [1.1.0] - 2021-03-26
### Added
- Uppdateringar efter ytterligare granskning
- Version 1.1

## [1.0.0] - 2020-10-09
### Added
- Stabil adress på lankadedata.se

### Changed
- Överflytt till HTML
- Inkluderade ändringar efter granskning
- Version 1.0

## [1.0.0] - 2020-06-05
### Added
- Genomgång av ett fåtal externa granskare
- Appendix A, B skrivna

### Changed
- Uppdaterad datamodell & scheman

## [0.5.0] - 2019-09-05
### Added
- Genomgång av befintliga publiceringar från Örebro, Lidingö och Göteborg.
- Definiera referenser allmängiltiga för Kommuner och Myndigheter.
- Sammanfattande kapitel om formatet CSV.

[Unreleased]: 
