# Data Specification for Accounts Payable

The data specification "Leverantörsreskontra" (Accounts Payable) is a Data Specification for public spenditure showing spending data from incoming invoices.

## Usage

Run these commands in your preferred terminal in a good folder for development:

```
git clone https://gitlab.com/lankadedata/spec/levreskontra.git

```
Open the file

```
index.html or schema.sjon

```
And then edit.

## Code of Conduct 

Please read our [Code of Conduct](CODE_OF_CONDUCT.md) before proceeding.

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

See more under [[CONTRIBUTING](CONTRIBUTING.md)]

## License

[CC-BY-4.0](https://choosealicense.com/licenses/cc-by-4.0/)

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.0-4baaaa.svg)](CODE_OF_CONDUCT.md) 
