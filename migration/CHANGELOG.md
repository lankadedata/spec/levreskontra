# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/sv/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

- Unreleased

## [Unreleased]

### Added
- Lägger till exempel på datamängd i CSV-, ODS- och XLSX-format.

### Fixed
- Bugg från komma till semikolon i exempel.

## [1.1.1] - 2021-03-26
### Added
- Lägger till [CONTRIBUTING.md](CONTRIBUTING.md)

### Fixed
- Fel text vid CODE_OF_CONDUCT.md [a99bbc1fbf2aef15210a60e93af4aeca90627b24](https://gitlab.com/lankadedata/spec/levreskontra/-/commit/a99bbc1fbf2aef15210a60e93af4aeca90627b24)
- Lägger till korrekt bidragsgivare [69c3d2989567b8b31703ec7c29808c365c8b7477](https://gitlab.com/lankadedata/spec/levreskontra/-/commit/69c3d2989567b8b31703ec7c29808c365c8b7477)
### Changed
- Migrering från Versionshistorik till CHANGELOG.md

## [1.1.0] - 2021-03-26
### Added
- Uppdateringar efter ytterligare granskning
- Version 1.1

## [1.0.0] - 2020-10-09
### Added
- Stabil adress på lankadedata.se

### Changed
- Överflytt till HTML
- Inkluderade ändringar efter granskning
- Version 1.0

## [1.0.0] - 2020-06-05
### Added
- Genomgång av ett fåtal externa granskare
- Appendix A, B skrivna

### Changed
- Uppdaterad datamodell & scheman

## [0.5.0] - 2019-09-05
### Added
- Genomgång av befintliga publiceringar från Örebro, Lidingö och Göteborg.
- Definiera referenser allmängiltiga för Kommuner och Myndigheter.
- Sammanfattande kapitel om formatet CSV.

[Unreleased]: 
