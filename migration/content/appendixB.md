# Exempel i JSON

Appendix B: Exempel på inköp i JSON

Exempel: Samma två inköp som för CSV fast nu i json.

```json
[
  {
    "kopare_id": "2120001710",
    "kopare": "Skövde kommun",
    "verifikationsnummer": "3921217387",
    "leverantor": "Jordbruksverket distriktsveterinärerna",
    "leverantor_id": "2021004151",
    "konto_nr": "7459",
    "konto_text": "övriga konsulttjänster",
    "belopp": "2022.31",
    "datum": "2020-01-01",
    "forvaltning": "392 - Park- och Naturnämnden",
    "fakturanummer": "",
    "grund": "",
    "avtal": "",
    "kommun_id": "",
    "s_kod_nr": ""
  },
  {
    "kopare_id": "2120001710",
    "kopare": "Skövde kommun",
    "verifikationsnummer": "8001020462",
    "leverantor": "Folkhälsomyndigheten",
    "leverantor_id": "2021006545",
    "konto_nr": "7499",
    "konto_text": "Övriga främmande tjänster",
    "belopp": "444.75",
    "datum": "2020-01-01",
    "forvaltning": "800 - Miljö- och klimatnämnden",
    "fakturanummer": "",
    "grund": "",
    "avtal": "",
    "kommun_id": "",
    "s_kod_nr": ""
  }
]
```
