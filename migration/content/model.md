# Datamodell

Datamodellen är tabulär, där varje rad motsvarar exakt en kontering och varje kolumn motsvarar en egenskap för detta inköp. 15 kolumner är definierade, där de första 9 är obligatoriska, om köparen är en kommun bör kolumn 14, kommun_id, som innehåller kommunkod anges.

<div class="note" title="Observation 1">
Vi väljer att använda beskrivande men korta kolumnnamn som uttrycks med gemener, utan mellanslag (understreck för att separera ord) och utan å, ä och ö. Orsaken till detta är:
</div>

1. Man vill använda kolumnnamn snarare än ordningen för att detektera om viss data finns. Korta och enkla kolumnnamn minskar risken för att problem uppstår.
2. Det är vanligt att man erbjuder söktjänster där frågor mot olika kolumner skrivs in i webbadresser och det är bra om det kan göras utan speciella teckenkodningar.

<div class="note" title="Observation 2">
Datamodellen är tabulär, där varje rad motsvarar exakt en kontering och varje kolumn motsvarar en egenskap för detta inköp. 15 kolumner är definierade, där de första 9 är obligatoriska, om köparen är en kommun bör kolumn 14, kommun_id, som innehåller kommunkod anges.
</div>

<div class="note" title="Observation 3">
Kolumn 3, verifikationsnummer, är en förutsättning för att den egna organisationen enkelt kunna göra en automatiserad förfrågan i en e-tjänst om begäran av fakturakopia.
</div>

<div class="ms_datatable">

| Namn  | Kardinalitet      | Datatyp                         | Beskrivning|
| -- | :---------------:| :-------------------------------: | ---------------------- |
|[**kopare_id**](#kopare_id)|1|text|**Obligatoriskt** - Ange organisationsnummer för inköpande organisation.|
|[**kopare**](#kopare)|1|text|**Obligatoriskt** - Ange organisationsnamnet på köparen.|
|[**verifikationsnummer**](#verifikationsnummer)|1|text|**Obligatoriskt** - Ange en unik och stabil identifierare.|
|[**leverantor**](#leverantor)|1|text|**Obligatoriskt** - Ange namnet på leverantören.|
|[**leverantor_id**](#leverantor_id)|1|text|**Obligatoriskt** - Ange organisationsnummer för levererande organisationen.|
|[**konto_nr**](#konto_nr)|1|text|**Obligatoriskt** - Ange kontonummer där inköpet har bokats.|
|[**konto_text**](#konto_text)|1|text|**Obligatoriskt** - Ange texten på konto där inköpet har bokats.|
|[**belopp**](#belopp)|1|decimal|**Obligatoriskt** - Ange beloppet i svenska kronor utan moms.|
|[**datum**](#datum)|1|dateTime|**Obligatoriskt** - Ange datum när fakturan registrerats.
|[forvaltning](#forvaltning)|0..1|text|**Obligatoriskt** - Ange namn på förvaltningen som har gjort inköpet.|
|[fakturanummer](#fakturanummer)|0..1|text|**Obligatoriskt** - Ange leverantörens fakturanummer så som den representeras i systemet.|
|[grund](#grund)|0..1|D&vert;R&vert;U&vert;A|**Obligatoriskt** - Referens till vilken grund inköpet har gjorts på.|
|[avtal](#avtal)|0..1|url|**Obligatoriskt** - Länk som identifierar avtal eller lagrum som grund för inköp.|
|[kommun_id](#kommun_id)|0..1|text|**Obligatoriskt** - Ange SCB:s kommunkod för er kommun eller region.|
|[s_kod_nr](#s_kod_nr)|0..1|text|**Obligatoriskt** - Ange [S-koden] som inköpet rapporterats på.|

</div>

## Förtydligande av datatyper

En del av datatyperna nedan förtydligas med hjälp av det som kallas reguljära uttryck. Dessa är uttryckta så att de matchar exakt, dvs inga inledande eller eftersläpande tecken tillåts.

### **heltal**
Reguljärt uttryck: **`/^\-?\\d+$/`**

Heltal anges alltid som en radda siffror utan mellanrum eventuellt med ett inledande minus. Se [xsd:integer](https://www.w3.org/TR/xmlschema-2/#integer) för en längre definition. Utelämnat värde tolkas aldrig som noll (0) utan tolkas som “avsaknad av värde”.
 
### **decimal**
Reguljärt uttryck: **`/^\-?\\d+\\.\\d+$/`**

Decimaltal anges i enlighet med [xsd:decimal](https://www.w3.org/TR/xmlschema-2/#decimal). Notera att i Sverige används ofta decimalkomma inte punkt. För att vara enhetlig mellan olika dataformat ska decimalpunkt användas då den tabulära modellen använder komma som separator.

Den kanoniska representationen i xsd:decimal är påbjuden, d.v.s. inga inledande nollor eller +, samt att man alltid ska ha en siffra innan och efter decimalpunkt. Noll skrivs som 0.0 och ett utelämnat värde skall aldrig tolkas som noll (0) utan “avsaknad av värde”.

### **url**

En länk till en webbsida där entiteten presenteras hos den lokala myndigheten eller organisationen.

Observera att man inte får utelämna schemat, d.v.s. "www.example.com" är inte en tillåten webbadress, däremot är "https://www.example.com" ok. Relativa webbadresser accepteras inte heller. (Ett fullständigt reguljärt uttryck utelämnas då den är både för omfattande och opedagogisk.)

Om du behöver ange flera URL:er måste du då sätta dubbelt citattecken och separera de olika URL:erna med kommatecken. Läs gärna med i RFC 4180 för CSV.

Kontrollera även innan du publicerar filen att det går att läsa in din fil utan problem. Det finns flera webbverktyg för att testa så kallad [parsing](https://sv.wikipedia.org/wiki/Parser) t.ex. [https://CSVLint.io](https://csvlint.io) som du kan använda för att testa din fil.

### **boolean**

Reguljärt uttryck: **`/^[true|false]?&/`**

I samtliga förekommande fall kan texten “true” eller “false” utelämnas.

Ett tomt fält skall tolkas som “okänt” eller “ej inventerat”. Ange enbart “true” eller “false” om du vet att egenskapen finns (true) eller saknas (false). Gissa aldrig.

Attributet är en så kallad Boolesk” datatyp och kan antingen ha ett av två värden: TRUE eller FALSE, men aldrig båda.

### **dateTime** 

Reguljärt uttryck: **`\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d(?:\.\d+)?Z?`**

dateTime värden kan ses som objekt med heltalsvärdena år, månad, dag, timme och minut, en decimalvärderad sekundegenskap och en tidszonegenskap.

[XML schema](https://www.w3.org/TR/xmlschema-2/#dateTime) 
</br>

[IS0 8601](https://en.wikipedia.org/wiki/ISO_8601)

Värden som kan anges för att beskriva ett datum:

YYYY-MM-DD
</br>
YYYY-MM
</br>
YYYYMMDD 
</br>
</div>

Observera att ni inte kan ange: **YYYYMM**    

För att beskriva tider uttrycker ni det enligt nedan:

**T** representerar tid, **hh** avser en nollställd timme mellan 00 och 24, **mm** avser en nollställd minut mellan 00 och 59 och **ss** avser en nollställd sekund mellan 00 och 60.

Thh:mm:ss.sss eller	Thhmmss.sss 
</br>
Thh:mm:ss eller Thhmmss
</br>
Thh:mm eller Thhmm
</br>

</div>

Exempel - XX som äger rum kl 17:00 den 22 februari 2023 

</br>
2024-02-22T17:00

</br>
</br>

20240222T1700

</div>

## Förtydligande av attribut

### kopare_id

Reguljärt uttryck: **`/^[a-zA-Z_:1-9]*$/`**

Ange organisationsnummer för inköpande organisation utan mellanslag/bindestreck [Exempel: "2120001710" för Skövde kommun].

### kopare

Ange organisationsnamnet på köparen. Förslagsvis stor bokstav först och sen gemener. [Exempel: "Skövde kommun"].

### verifikationsnummer

Ange en unik och stabil identifierare för ett verifikationsnummer. Förslagsvis det verifikationsnummer som finns i ekonomisystemet. Undvik mellanslag och andra tecken som inte passar bra i en URI (webbadress).

### leverantor

Ange namnet på leverantören. Förslagsvis stor bokstav först och sen gemener, men specifika skrivningar av leverantörers namn bör respekteras bara det sker konsekvent.

### leverantor_id

Ange organisationsnummer för levererande organisation utan mellanslag/bindestreck [Exempel: "5560360793” Saab AB].

### konto_nr

Ange kontonummer där inköpet har bokats enligt den kontoplan som används tex “64310” för facklitteratur.

### konto_text

Ange texten på konto där inköpet har bokats enligt den kontoplan som används tex “facklitteratur”.. 

### belopp

Ange beloppet i svenska kronor utan moms.

### datum

Ange datum när fakturan registreras enligt “2012-06-14” utan mellanslag.

### forvaltning

Ange namn på förvaltningen som har gjort inköpet. T.ex. "Ekonomikontoret". Förslagsvis stor bokstav först och sen gemener, men specifika skrivningar av förvaltningens namn bör respekteras bara det sker konsekvent.

### fakturanummer

Ange leverantörens fakturanummer så som den representeras i systemet. Detta blir inte en unik identifierare då två leverantörer kan använda samma fakturanummer.

### grund

Referens till vilken grund inköpet har gjorts på, antingen Direkt, via Ramavtal, Upphandling eller på Annat sätt.

### avtal

Länk som identifierar avtal eller lagrum som grund för inköp, använd i första hand webbadresser som inleds med http:// eller https://.

### kommun_id

Ange [SCB:s kommunkod för er kommun eller region](https://www.dataportal.se/sv/datasets/653_26835/kommunkod-kommunnamn-folkmangd-20201231-skr-s-kommungrupp-lan-och-region). Exempel: "0163" för Sollentuna kommun eller "12" för Region Skåne. Observera att en kommunkod måste vara exakt 4 siffror lång för en kommun och exakt 2 siffror lång för en region, inledande nollor får alltså inte tas bort.

### s_kod_nr

Ange den inrapporteringskod [S-koden](https://www.esv.se/statens-ekonomi/redovisning/baskontoplans-koder/) som inköpet rapporterats på enligt ordningen för statsredovisningen i statens informationssystem Hermes exempelvis 'S5353' för tryckning, publikationer och pappersvaror, utgifter, utomstatliga.


 




