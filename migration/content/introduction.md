# Introduktion 

Denna specifikation definierar en enkel tabulär informationsmodell för inköp. Specifikationen innefattar också en beskrivning av hur informationen uttrycks i formatet CSV, JSON och länkade data (RDF). Som grund förutsätts CSV kunna levereras då det är praktiskt format och skapar förutsägbarhet för mottagare av informationen. Det är också ett format som gör datautdrag enklare då snarlika processer ofta finns.

Ett inköp bokas ibland på flera olika konton då ett inköp kan innehålla olika typer av kostnader. Detta gör att ett inköp kan motsvara flera rader.

Uttrycket i länkade data återanvänder konstruktioner från andra initiativ och ger därmed en grund för interoperabilitet med data som uttrycks utan kunskap om denna specifikation.

Det rekommenderas att publicera månadsvisa dataleveranser i form av CSV och låta en datamängd definieras av ett kalenderår. Detta dels för att tagarna ofta använder tabelldata och för att det mellan ett kalenderår till nästa kan uppstå överlappande information.