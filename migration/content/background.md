**Bakgrund**

Med leverantörsreskontra menas uppgifter som återfinns om gjorda inköp såsom värde på inköp, leverantörsnamn, inköpande organisation och identifierare till underlag för inköpet. I texten nedan används för enkelhets skull ordet “ett inköp” för att benämna detta.

Följande har deltagit:

**[Dataverkstad](https://www.vgregion.se/ov/dataverkstad/)** - Modellering och rådgivning.<br>


