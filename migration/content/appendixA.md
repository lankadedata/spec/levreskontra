## Exempel i CSV, kommaseparerad

Exempel: Nedan är en fil med två riktiga inköp från Göteborg stad, fälten grund och avtal innehåller påhittade värden dock.

<div class="example csvtext">
kopare_id,kopare,verifikationsnummer,leverantor,leverantor_id,konto_nr,konto_text,belopp,datum,forvaltning,fakturanummer,grund,avtal,kommun_id,s_kod_nr
2120001355,3921217387,Jordbruksverket distriktsveterinärerna,2021004151,7459,övriga konsulttjänster,2022.31,2020-01-01,392 - Park- och Naturnämnden,,,,,
2120001355,8001020462,Folkhälsomyndigheten,7499,Övriga främmande tjänster,444.75,2020-01-01,2021006545,800 - Miljö- och klimatnämnden,,,,,

<br>
Exempel i CSV
</div>

