# Data Specification

The X data specification is a Data Specification for ...

## Installation

1.  Choose preferred package manager.
2.  Install `git`
3.  Install `npm`
4.  Install `live-server`. Is preferably done with `sudo npm -g install live-server` on Linux-systems so that it works for all users on the system.

## Usage

Run these commands in your preferred terminal in a good folder for development:

```
git clone https://gitlab.com/lankadedata/spec/EXAMPLE.git

```

Run:

```
./build.sh

```
Then run
```
live-server

```

## Code of Conduct 

Please read our [Code of Conduct](CODE_OF_CONDUCT.md) before proceeding.

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

See more under [[CONTRIBUTING](CONTRIBUTING.md)]

## License

[CC-BY-4.0](https://choosealicense.com/licenses/cc-by-4.0/)
