# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/sv/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### To address
- Användning av ePo Ontology för att beskriva grunder till inköp och FIBO Ontology för att definiera egenskaper i schema.json för att särskilja aktörer och id.  
- Byta datatyper för kopare_id, leverantor_id, konto_nr 
- Undersök s_kod_nr för dess koppling till räkenskapssammandraget
- Undersök grund och vilka som värden som ska antas från ePO ontology

## [1.2.0]
### Added
- Lägger till CONTRIBUTING.md
- Kolumn för kardinalitet i datamodell

### Changed
- Migrering från Versionshistorik till CHANGELOG.
- Uppdaterad översättning av CODE_OF_CONDUCT till svenska.
- Uppdaterad schema.json med propertyURL, datatype m.m.
### Fixed
- Bugg från komma till semikolon i exempel.
- Rättar till lista över Bidragsgivare från remissprocesser.
- Tydligare hänvisning till licens CC-BY 4.0.

### Removed
- Borttagen logotyp från MetaSolutions tidigare utgåva fram till 1.1.1.

## [1.1.1]

### Fixed
- 2023-11-21 - Tredje publicerade stabila versionen 1.1.1
- Fixar efter ytterligare granskning med tillagda bidragsgivare

## [1.1.0] - 2021-03-26
### Added
- Uppdateringar efter ytterligare granskning
- Version 1.1

## [1.0.0] - 2020-10-09
### Added
- Stabil adress på lankadedata.se

### Changed
- Överflytt till HTML
- Inkluderade ändringar efter granskning
- Version 1.0

## [1.0.0] - 2020-06-05
### Added
- Genomgång av ett fåtal externa granskare
- Appendix A, B skrivna

### Changed
- Uppdaterad datamodell & scheman

## [0.5.0] - 2019-09-05
### Added
- Genomgång av befintliga publiceringar från Örebro, Lidingö och Göteborg.
- Definiera referenser allmängiltiga för Kommuner och Myndigheter.
- Sammanfattande kapitel om formatet CSV.

[Unreleased]: 
