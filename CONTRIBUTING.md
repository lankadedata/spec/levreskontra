# Riktlinjer för Bidragsgivare (Contributor Guidelines)

## Råd till nya bidragsgivare (Advice for new contributors)

Börja litet. De ändringsförlsag (PR) som har störtst chans att komma med är de som gör små ändringar som är enkla att verifiera, med tydliga och specifika intentioner. Se nedan för mer [riktlinjer om ändringsförslag](#pull-requests).

Det är en bra idé att omsätta ditt intresse i utvecklingsarbetet genom att hitta det som existerande [ärende](https://gitlab.com/lankadedata/spec/levreskontra/-/issues) för det eller genom att skapa ett nytt ärende själv. Du kan också använda det ärendet som en plats för att signalera dina intentioner och få feedback från de användare som är mest troliga att uppskatta dina ändringsförslag.

När du spenderat lite av din tid på att planera din lösning så är det bra att gå tillbaka till ärendet och beskriva ditt lösningsförslag till ändringen. Vi lämnar gärna feedback och diskuterar. [An ounce of prevention, as they say!](https://www.goodreads.com/quotes/247269-an-ounce-of-prevention-is-worth-a-pound-of-cure)

### Bidra till innehåll - utan kod och genom diskussion

Titta på [ärende](https://gitlab.com/lankadedata/spec/levreskontra/-/issues) och diskutera. Lägg till nytt ärende vid behov.

### Bidra till kod - Installera för att testa dina ändringsförslag

1.  Välj valfri pakethanterare.
2.  Installera `git`
3.  Installera `npm`
4.  Installera `live-server`. Görs föredragsvis med `sudo npm -g install live-server` på Linux-system så att det fungerar för alla användare på systemet.

### Alla plattformar

Sen kör dessa kommandon i din föredragna terminal i en bra mapp för utveckling:


```
git clone https://gitlab.com/lankadedata/spec/levreskontra.git

```
Öppna filen

```
index.html eller schema.sjon

```
Titta på genererad index.html.

## Pull requests (Ändringsförslag)

Vill du skapa ett ändringsförslag? Vänligen observera följande riktlinjer. 

- Först, se till att din ./build.sh körning passerar och visar senaste versionen du hämtat hem.
- Kör [rebase](https://nathanleclaire.com/blog/2014/09/14/dont-be-scared-of-git-rebase/) 
för dina ändringar på den senaste `main`-grenen och lös eventuella konflikter.
  Detta försäkrar dig om att dina förändringar kommer sammanföras enkelt när du öppnar ditt ändringsförslag (PR).
- Försäkra dig om att allt funkar och kör test!
- Försäkra dig om att diffen mellan vår main-gren och din gren endast innehåller de minimala mängden av behövda förändringar för att implementera din funktion eller buggfix. Detta kommer göra det enklare för personen som granskar din kod för att godkänna förändringarna. Vänligen skicka inte in ett ändringsförslag (PR) med utkommenterad kod eller oavslutade funktionre.
- Undvik meningslösa eller för granulära bidrag. Om din gren innehåller bidrag med rader som "Oops, återställde denna förändring" eller "Experimenterar bara, kommer radera senare", vänligen [mosa/squasha eller rebase:a bort dessa ändringar](https://robots.thoughtbot.com/git-interactive-rebase-squash-amend-rewriting-history).
- Ha inte för få birag. Om du har en komplicerad eller större ändringsförslag på din gren så kan det vara förnuftigt att bryta upp de ändringarna i logiska atomiska bitar för att stötta i granskningsprocessen.
- Tillhandahåll en välskriven och trevligt formaterat bidragsmeddelande. Se [denna länk](http://chris.beams.io/posts/git-commit/)
  för några tips på formattering. Så lång som innehåll går, försöka inkludera i din sammanfattning:
  1.  Vad du ändrat
  2.  Varför denna ändring gjordes (inklusive git-ärendets # om möjligt)
  3.  Några relevanta tekniska detaljer eller motivationer för dina val vid implementation som kan vara hjälpsamma för någon som granskar eller reviderar historiken över bidrag i framtiden. När du tvivlar, kör err på sidan av ett längre commit-meddelnade.

Spendera framförallt lite tid med kodförrådet. Följ vår (kommande) mall för pull requests som kan läggas till för ditt ändringsförslag automatiskt. Ta en koll på nyligen införda ändringsförslag och titta på hur de gjorde saker.
